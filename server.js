var cluster = require('cluster');       //

if (cluster.isMaster) {
    var os          = require('os');
    var cpuCount    = os.cpus().length;

    // Create a worker for each CPU
    for (var i = 0; i < cpuCount; ++i) {
        cluster.fork();
    }

    cluster.on('exit', function (worker) {
        // Replace the dead worker
        console.log('Worker %d died :(', worker.id);
        cluster.fork();
    });

} else {
    // set up ========================
    var express         = require('express');           //
    var app             = express();                    // create our app w/ express
    var port            = process.env.PORT || 8080;     // port number
    var mongoose        = require('mongoose');          // mongoose for mongodb
    var morgan          = require('morgan');            // log requests to the console (express4)
    var bodyParser      = require('body-parser');       // pull information from HTML POST (express4)
    var methodOverride  = require('method-override');   // simulate DELETE and PUT (express4)
    var passport        = require('passport');          //
    var flash           = require('connect-flash');     //
    var cookieParser    = require('cookie-parser');     //
    var cookieSession   = require('cookie-session');    //
    var session         = require('express-session');   //
    var favicon         = require('serve-favicon');     //
    var csrf            = require('csurf');             //

    var mongoStore      = require('connect-mongo')(session);

    var configDB = require('./config/database.js');     //

    // configuration =================

    mongoose.connect(configDB.url);

    app.use(cookieSession({
      name: 'Tuto1-login',
      secret: 'aS0fWzGMpNAns8xeTuF9'
    }));

    // required for passport
    app.use(session({
        store: new mongoStore({ mongooseConnection: mongoose.connection }),
        secret: 'aS0fWzGMpNAns8xeTuF9',
        saveUninitialized: false,
        resave: false,
        duration: 30 * 60 * 1000,
        cookie: { maxAge: 5 * 60 * 1000 },
        activeDuration: 5 * 60 * 1000
    })); // session secret

    app.use(favicon(__dirname + '/public/images/favicon.ico'));
    app.use(express.static(__dirname + '/public'));                 // set the static files location /public/img will be /img for users
    app.use(morgan('dev'));                                         // log every request to the console
    app.use(bodyParser.urlencoded({'extended':'true'}));            // parse application/x-www-form-urlencoded
    app.use(bodyParser.json());                                     // parse application/json
    app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
    app.use(methodOverride());                                      //
    app.use(cookieParser('Tuto1-login'));                           //

    var favicon = require('serve-favicon');

    app.set('view engine', 'pug');

    app.use(passport.initialize());
    app.use(passport.session());                                    // persistent login sessions
    app.use(flash());                                               // use connect-flash for flash messages stored in session

    require('./config/passport')(passport);

    app.use(csrf({ cookie: true }));
    app.use(function(req, res, next) {
        res.cookie('XSRF-TOKEN', req.csrfToken());
        res.locals.csrftoken = req.csrfToken();
        next();
    });
    app.use(function (err, req, res, next) {
        if (err.code !== 'EBADCSRFTOKEN') return next(err)

        // handle CSRF token errors here
        res.status(403)
        res.send('session has expired or form tampered with')
    });

    // routes ======================================================================
    require('./app/routes.js')(app, passport); // load our routes and pass in our app and fully configured passport

    // listen (start app with node server.js) ======================================
    app.listen(port);
    console.log("Worker %d listening on port %d", cluster.worker.id, port);
}
