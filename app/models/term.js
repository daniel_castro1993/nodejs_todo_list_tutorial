var mongoose  = require('mongoose');
var Schema    = mongoose.Schema;

var termSchema = new Schema({
  term        : String,
  definitions : [String],
  related     : [Schema.ObjectId],
  synonyms    : [Schema.ObjectId],
  antonyms    : [Schema.ObjectId],
  date        : { type: Date, default: Date.now }
});

module.exports = mongoose.model('Term', termSchema);
