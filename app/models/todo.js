var mongoose = require('mongoose');

var todoSchema = mongoose.Schema({
    text : String,
    date : { type: Date, default: Date.now }
});

module.exports = mongoose.model('Todo', todoSchema);
