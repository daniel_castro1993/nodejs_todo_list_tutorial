module.exports = function(app, passport) {

    // TODOs tutorial routes
    require('./routes/todos/todos')(app, passport);

    // Login routes
    require('./routes/login/login')(app, passport);

    // Authorize routes
    require('./routes/login/authorize')(app, passport);

    // Unlink accounts routes
    require('./routes/login/unlink')(app, passport);
};
