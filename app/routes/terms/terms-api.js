var Term = require('../../app/models/term');

module.exports = function(app, passport) {

  // =====================================
  // SEARCH A TERM =======================
  // =====================================
  app.get('/api/term/search/:term_search', function(req, res) {
    Term.find({
      term: {
        $regex: req.params.term_search,
        $options: 'i'
      }
    }, function(err, terms) {
      if (err)
        res.send(err)

      res.json(terms);
    });
  });

  app.post('/api/term/insert', function(req, res) {
      // create a todo, information comes from AJAX request from Angular
      Term.create({
          term : req.body.term,
          definitions : req.body.definitions
      }, function(err, todo) {
          if (err)
              res.send(err);

          // get and return all the todos after you create another
          Todo.find(function(err, terms) {
              if (err)
                  res.send(err)
              res.json(terms);
          });
      });

  });

  // delete a todo
  /*app.delete('/api/todos/:todo_id', function(req, res) {
      Todo.remove({
          _id : req.params.todo_id
      }, function(err, todo) {
          if (err)
              res.send(err);

          // get and return all the todos after you create another
          Todo.find(function(err, todos) {
              if (err)
                  res.send(err)
              res.json(todos);
          });
      });
  });

  // application -------------------------------------------------------------
  app.get('/todos', function(req, res) {
      res.render('todos');
  });*/
}
