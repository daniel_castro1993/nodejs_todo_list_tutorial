// load all the things we need
var LocalStrategy       = require('passport-local').Strategy;
var FacebookStrategy    = require('passport-facebook').Strategy;
var TwitterStrategy     = require('passport-twitter').Strategy;
var GoogleStrategy      = require('passport-google-oauth').OAuth2Strategy;

// load up the user model
var User                = require('../app/models/user');

// load the auth variables
var configAuth = require('./auth');

// if there are multiple repeated accounts they get merged
function mergeMultipleUsers(users) {

  if(!users || !(users.length > 1)) {
    return;
  }

  for (var i = 1; i < users.length; ++i) {
    var user = users[i];
    if(!users[0].local) {
      users[0].local = users[i].local;
    }
    if(!users[0].facebook) {
      users[0].facebook = users[i].facebook;
    }
    if(!users[0].twitter) {
      users[0].twitter = users[i].twitter;
    }
    if(!users[0].google) {
      users[0].google = users[i].google;
    }
    users[i].remove(function(err) {
      if (err)
        throw err;
    });
  }

  users[0].save(function(err) {
    if (err)
      throw err;
  });
}

// expose this function to our app using module.exports
module.exports = function(passport) {

  // =========================================================================
  // passport session setup ==================================================
  // =========================================================================
  // required for persistent login sessions
  // passport needs ability to serialize and unserialize users out of session

  // used to serialize the user for the session
  passport.serializeUser(function(user, done) {
    done(null, user.id);
  });

  // used to deserialize the user
  passport.deserializeUser(function(id, done) {
    User.findById(id, function(err, user) {
      done(err, user);
    });
  });

  // =========================================================================
  // LOCAL SIGNUP ============================================================
  // =========================================================================
  // we are using named strategies since we have one for login and one for signup
  // by default, if there was no name, it would just be called 'local'

  passport.use('local-signup', new LocalStrategy({
    // by default, local strategy uses username and password, we will override with email
    usernameField : 'email',
    passwordField : 'password',
    passReqToCallback : true // allows us to pass back the entire request to the callback
  },
  function(req, email, password, done) {

    // asynchronous
    // User.findOne wont fire unless data is sent back
    process.nextTick(function() {

      // find a user whose email is the same as the forms email
      // we are checking to see if the user trying to login already exists
      User.find({ 'local.email' :  email }, function(err, users) {
        // if there are any errors, return the error
        if (err)
          return done(err);

        // check to see if there is already a user with that email
        if (users.length > 0) {
          var msg = 'That email is already taken.'
          req.flash('signupMessage', msg);
          return done(null, false, {message: msg});
        } else {

          // if there is no user with that email
          // create the user
          var newUser            = new User();

          // set the user's local credentials
          newUser.local.email     = email;
          newUser.local.password  = newUser.generateHash(password);
          newUser.facebook        = {};
          newUser.twitter         = {};
          newUser.google          = {};

          // save the user
          newUser.save(function(err) {
              if (err)
                  throw err;
              return done(null, newUser);
          });
        }
      });
    });
  }));


  // =========================================================================
  // LOCAL LOGIN =============================================================
  // =========================================================================
  // we are using named strategies since we have one for login and one for signup
  // by default, if there was no name, it would just be called 'local'

  passport.use('local-login', new LocalStrategy({
    // by default, local strategy uses username and password, we will override with email
    usernameField : 'email',
    passwordField : 'password',
    passReqToCallback : true // allows us to pass back the entire request to the callback
  },
  function(req, email, password, done) { // callback with email and password from our form

    // asynchronous
    process.nextTick(function() {

      if (!req.user) {
        User.find({ 'local.email' :  email }, function(err, users) {
          // if there are any errors, return the error before anything else
          if (err)
            return done(err);

          if (!(users.length > 0)) {
            var msg = 'No user found.';
            req.flash('loginMessage', msg);
            return done(null, false, {message: msg});
          }

          mergeMultipleUsers(users);

          if (!users[0].validPassword(password)) {
            var msg = 'Oops! Wrong password.';
            req.flash('loginMessage', msg);
            return done(null, false, {message: msg});
          }

          var msg = 'Login success.';
          return done(null, users[0], {message: msg});
        });
      } else {

        User.find({ 'local.email' :  email }, function(err, users) {

          var user            = req.user;

          if (users.length > 0) {
            if(!users[0].facebook.token) {
              users[0].facebook = user.facebook;
            }
            if(!users[0].twitter.token) {
              users[0].twitter  = user.twitter;
            }
            if(!users[0].google.token) {
              users[0].google   = user.google;
            }

            user.remove(function (err) {
              if (err)
                throw err;
            });

            req.user = users[0];

            users[0].save(function(err) {
              if (err)
                throw err;
              return done(null, users[0]);
            });
          } else {
            // update the current users local credentials
            user.local.email    = email;
            user.local.password = user.generateHash(password);

            req.user = user;

            // save the user
            user.save(function(err) {
              if (err)
                throw err;
              return done(null, user);
            });
          }
        });
      }
    });
  }));


  // =========================================================================
  // FACEBOOK ================================================================
  // =========================================================================
  passport.use(new FacebookStrategy({

    // pull in our app id and secret from our auth.js file
    clientID        : configAuth.facebookAuth.clientID,
    clientSecret    : configAuth.facebookAuth.clientSecret,
    callbackURL     : configAuth.facebookAuth.callbackURL,
    passReqToCallback : true // allows us to pass in the req from our route (lets us check if a user is logged in or not)
  },
  // facebook will send back the token and profile
  function(req, token, refreshToken, profile, done) {

    process.nextTick(function() {

      // check if the user is already logged in
      if (!req.user) {
        // find the user in the database based on their facebook id
        User.find({ 'facebook.id' : profile.id }, function(err, users) {

          if (err)
            return done(err);

          // if the user is found, then log them in
          if (users.length > 0) {
            mergeMultipleUsers(users);

            if (!users[0].facebook || !users[0].facebook.token) {
              users[0].facebook.token = token;
              users[0].facebook.name  = profile.displayName;

              users[0].save(function(err) {
                if (err)
                  throw err;
                return done(null, users[0]);
              });
            }

            return done(null, users[0]);
          } else {

            var newUser             = new User();

            newUser.local           = {};
            newUser.facebook.id     = profile.id;
            newUser.facebook.token  = token;
            newUser.facebook.name   = profile.displayName
            newUser.twitter         = {};
            newUser.google          = {};

            newUser.save(function(err) {
              if (err)
                throw err;

              return done(null, newUser);
            });
          }
        });

    } else {
        var user            = req.user;

        user.facebook.id    = profile.id;
        user.facebook.token = token;
        user.facebook.name  = profile.displayName;

        user.save(function(err) {
          if (err)
            throw err;
          return done(null, user);
        });
      }
    });
  }));

  // =========================================================================
  // TWITTER =================================================================
  // =========================================================================
   passport.use(new TwitterStrategy({
      consumerKey     : configAuth.twitterAuth.consumerKey,
      consumerSecret  : configAuth.twitterAuth.consumerSecret,
      callbackURL     : configAuth.twitterAuth.callbackURL,
      passReqToCallback : true
  },
  function(req, token, tokenSecret, profile, done) {

      // asynchronous
      process.nextTick(function() {

          // check if the user is already logged in
          if (!req.user) {

              User.find({ 'twitter.id' : profile.id }, function(err, users) {
                  if (err)
                      return done(err);

                  if (users.length > 0) {

                      mergeMultipleUsers(users);

                      if (!users[0].twitter || !users[0].twitter.token) {
                          users[0].twitter.token       = token;
                          users[0].twitter.username    = profile.username;
                          users[0].twitter.displayName = profile.displayName;

                          users[0].save(function(err) {
                              if (err)
                                  throw err;
                              return done(null, users[0]);
                          });
                      }

                      return done(null, users[0]); // user found, return that user
                  } else {
                      // if there is no user, create them
                      var newUser                  = new User();

                      newUser.local                = {};
                      newUser.facebook             = {};
                      newUser.twitter.id           = profile.id;
                      newUser.twitter.token        = token;
                      newUser.twitter.username     = profile.username;
                      newUser.twitter.displayName  = profile.displayName;
                      newUser.google               = {};

                      newUser.save(function(err) {
                          if (err)
                              throw err;
                          return done(null, newUser);
                      });
                  }
              });

          } else {
              // user already exists and is logged in, we have to link accounts
              var user                 = req.user; // pull the user out of the session

              user.twitter.id          = profile.id;
              user.twitter.token       = token;
              user.twitter.username    = profile.username;
              user.twitter.displayName = profile.displayName;

              user.save(function(err) {
                  if (err)
                      throw err;
                  return done(null, user);
              });
          }
      });
  }));

  // =========================================================================
  // GOOGLE ==================================================================
  // =========================================================================
  passport.use(new GoogleStrategy({

      clientID        : configAuth.googleAuth.clientID,
      clientSecret    : configAuth.googleAuth.clientSecret,
      callbackURL     : configAuth.googleAuth.callbackURL,
      passReqToCallback : true // allows us to pass in the req from our route (lets us check if a user is logged in or not)

  },
  function(req, token, refreshToken, profile, done) {

      // asynchronous
      process.nextTick(function() {

        // check if the user is already logged in
        if (!req.user) {

          User.find({ 'google.id' : profile.id }, function(err, users) {
              if (err)
                return done(err);

              if (users.length > 0) {

                  mergeMultipleUsers(users);

                  // if there is a user id already but no token (user was linked at one point and then removed)
                  if (!users[0].google || !users[0].google.token) {
                      users[0].google.token = token;
                      users[0].google.name  = profile.displayName;
                      users[0].google.email = profile.emails[0].value;

                      users[0].save(function(err) {
                        if (err)
                          throw err;
                        return done(null, users[0]);
                      });
                  }
                  return done(null, users[0]);
              } else {
                var newUser           = new User();

                newUser.local         = {};
                newUser.facebook      = {};
                newUser.twitter       = {};
                newUser.google.id     = profile.id;
                newUser.google.token  = token;
                newUser.google.name   = profile.displayName;
                newUser.google.email  = profile.emails[0].value;

                newUser.save(function(err) {
                  if (err)
                    throw err;
                  return done(null, newUser);
                });
              }
            });
          } else {
            var user          = req.user;

            user.google.id    = profile.id;
            user.google.token = token;
            user.google.name  = profile.displayName;
            user.google.email = profile.emails[0].value;

            user.save(function(err) {
                if (err)
                    throw err;
                return done(null, user);
            });
          }
      });
  }));
};
