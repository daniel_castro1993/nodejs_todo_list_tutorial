var app = angular.module('app', []);

app.factory('$csrf', function () {
    var cookies = document.cookie.split('; ');
    for (var i=0; i<cookies.length; i++) {
        var cookie = cookies[i].split('=');
        if(cookie[0].indexOf('XSRF-TOKEN') > -1) {
            return cookie[1];
        }
    }
    return 'none';
});

app.factory('authInterceptor', ['$q', '$location', function($q, $location) {  
    var authInterceptor = {
        response: function(response) {
            // do something on success 
            return response;
        },
        responseError: function(response) {
            if (response.status === 401)
                $location.url('/login');
            return $q.reject(response);
        }
    };
    return authInterceptor;
}]);

app.config(['$httpProvider', function($httpProvider) {  
    $httpProvider.interceptors.push('authInterceptor');
}]);
