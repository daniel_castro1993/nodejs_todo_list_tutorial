function loginController($scope, $http, $location, $csrf) {
    $scope.formData = {};
    $scope.tokenValue = $csrf;

    $scope.login = function () {
        $http.post('/api/auth/login', $scope.formData)
            .success(function(data) {
                console.log(data);
                $scope.formData = {};
                $scope.user = data;
                window.location.href = "/profile";
            })
            .error(function(data) {
                console.log('Error: ' + data.message);
                $scope.message = data.message;
            });
    };

    $scope.logout = function () {
        $http.get('/logout', $scope.formData)
            .success(function(data) {
                console.log(data);
                $scope.formData = {};
                $scope.user = data;
                window.location.href = "/";
            })
            .error(function(data) {
                console.log('Error: ' + data.message);
                $scope.message = data.message;
            });
    };

    $scope.signup = function () {
        $http.post('/api/auth/signup', $scope.formData)
            .success(function(data) {
                console.log(data);
                $scope.formData = {};
                $scope.user = data;
                $window.location.href = "/profile";
            })
            .error(function(data) {
                console.log('Error: ' + data.message);
                $scope.message = data.message;
            });
    };

}

app.controller("loginController", loginController);
